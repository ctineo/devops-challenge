FROM node:lts-alpine
ENV NODE_ENV production
USER node
WORKDIR /usr/src/app
COPY --chown=node:node . .
COPY --chown=node:node . /usr/src/app
EXPOSE 8080
CMD [ "node", "index.js" ]